# Dockerfile for base image (use docker exec or a shell on the container)
FROM tensorflow/tensorflow:1.13.1-gpu-py3

RUN apt-get update && apt-get install -y vim && \
	python3 -m pip --no-cache-dir install --upgrade \
        grpcio-tools==1.20.1 \
        tqdm nltk langdetect omegaconf sklearn && \
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
	ldconfig && \
	apt-get clean && \
	apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/*

ARG WORKSPACE=/mrc

RUN mkdir ${WORKSPACE}
COPY . ${WORKSPACE}

VOLUME ${WORKSPACE}/models

WORKDIR ${WORKSPACE}
