#!/bin/bash

# Generate proto
bash scripts/build_proto.sh

export CUDA_VISIBLE_DEVICES=5

MODEL_PATH="models/sample"
PORT=35000

python src/server.py \
    --model_path ${MODEL_PATH} \
    --port ${PORT} \
