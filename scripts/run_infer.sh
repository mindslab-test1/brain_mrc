#!/bin/bash
export CUDA_VISIBLE_DEVICES=5

MODEL_PATH="models/sample"

python src/runner.py --run infer \
    --model_path ${MODEL_PATH} \
    --infer_context "이순신은 조선의 명장이다" \
    --infer_question "이순신은 누구인가?" \

