#!/bin/bash
export CUDA_VISIBLE_DEVICES=5

MODEL_PATH="models/korquad_l3e5/model.ckpt-5002"

python src/runner.py --run test \
    --model_path ${MODEL_PATH} \
    --log_level DEBUG
