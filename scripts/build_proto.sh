#!/bin/bash

PROTO_PARENT='src'
python -m grpc.tools.protoc --proto_path ${PROTO_PARENT} \
    ${PROTO_PARENT}/proto/mrc.proto \
    --python_out ${PROTO_PARENT} \
    --grpc_python_out ${PROTO_PARENT} \
