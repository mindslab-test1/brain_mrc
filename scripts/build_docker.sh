#!/bin/bash

# !!CHECK!! below 3 lines
PRETRAINED_CKPT="/raid/pretrained/bert_kor_mecab"
ENGINE_NAME="mrc"
VERSION=1.1.1


WRAP_CONTAINER="wrap_${ENGINE_NAME}-${VERSION}"

REGISTRY="docker.maum.ai:443"
IMAGE_NAME="brain/brain_${ENGINE_NAME}"
BASE_IMAGE="${REGISTRY}/${IMAGE_NAME}:${VERSION}"
SERVER_IMAGE="${BASE_IMAGE}-server"
PACKED_IMAGE="${BASE_IMAGE}-packed"

docker build -f Dockerfile -t ${BASE_IMAGE} . 
docker build -f Dockerfile-server -t ${SERVER_IMAGE} .

docker run -it -d --name ${WRAP_CONTAINER} ${BASE_IMAGE} 
docker exec ${WRAP_CONTAINER} mkdir /pretrained
docker cp ${PRETRAINED_CKPT} ${WRAP_CONTAINER}:/pretrained/bert_kor_mecab
docker commit ${WRAP_CONTAINER} ${PACKED_IMAGE}

docker stop ${WRAP_CONTAINER} 
docker rm ${WRAP_CONTAINER}

# Run below when you are sure with the images
# docker push ${BASE_IMAGE}
# docker push ${SERVER_IMAGE}
# docker push ${PACKED_IMAGE}
# docker rmi ${IMAGES}
