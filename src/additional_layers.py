import tensorflow as tf

init = {
    0: tf.truncated_normal_initializer(stddev=0.02),
    1: tf.contrib.layers.xavier_initializer(),
    2: tf.initializers.he_normal()
}


def ae_decoder(x, latent_size, is_training):
    if is_training:
        dropout = 0.1
    else:
        dropout = 0

    hidden_size = int(x.shape[-1])
    initializer = init[0]

    latent_weights = tf.get_variable(
        "cls/squad/latent_weights", [latent_size, hidden_size],
        initializer=initializer)

    latent_bias = tf.get_variable(
        "cls/squad/latent_bias", [latent_size], initializer=tf.zeros_initializer())

    new_sequence_weights = tf.get_variable(
        "cls/squad/new_sequence_weights", [hidden_size, latent_size],
        initializer=initializer)

    new_sequence_bias = tf.get_variable(
        "cls/squad/new_sequence_bias", [hidden_size], initializer=tf.zeros_initializer())

    latent_matrix = tf.matmul(x, latent_weights, transpose_b=True)
    latent_matrix = tf.nn.dropout(tf.nn.sigmoid(tf.nn.bias_add(latent_matrix, latent_bias)), keep_prob=(1-dropout))

    new_sequence_matrix = tf.matmul(latent_matrix, new_sequence_weights, transpose_b=True)
    new_sequence_matrix = tf.nn.dropout(tf.nn.bias_add(new_sequence_matrix, new_sequence_bias), keep_prob=(1-dropout))

    ae_loss = tf.reduce_mean(tf.pow(x - new_sequence_matrix, 2))

    return new_sequence_matrix, ae_loss


def vae_decoder(x, latent_size, z_size, is_training):
    if is_training:
        dropout = 0.1
    else:
        dropout = 0

    hidden_size = int(x.shape[-1])
    initializer = init[0]

    z_weights = tf.get_variable(
        "cls/squad/z_weights", [latent_size, hidden_size],
        initializer=initializer)

    z_bias = tf.get_variable(
        "cls/squad/z_bias", [latent_size], initializer=tf.zeros_initializer())

    z_m_weights = tf.get_variable(
        "cls/squad/z_m_weights", [z_size, latent_size],
        initializer=initializer)

    z_m_bias = tf.get_variable(
        "cls/squad/z_m_bias", [z_size], initializer=tf.zeros_initializer())

    z_s_weights = tf.get_variable(
        "cls/squad/z_s_weights", [z_size, latent_size],
        initializer=initializer)

    z_s_bias = tf.get_variable(
        "cls/squad/z_s_bias", [z_size], initializer=tf.zeros_initializer())

    latent_weights = tf.get_variable(
        "cls/squad/latent_weights", [latent_size, z_size],
        initializer=initializer)

    latent_bias = tf.get_variable(
        "cls/squad/latent_bias", [latent_size], initializer=tf.zeros_initializer())

    new_sequence_weights = tf.get_variable(
        "cls/squad/new_sequence_weights", [hidden_size, latent_size],
        initializer=initializer)

    new_sequence_bias = tf.get_variable(
        "cls/squad/new_sequence_bias", [hidden_size], initializer=tf.zeros_initializer())

    z_matrix = tf.matmul(x, z_weights, transpose_b=True)
    z_matrix = tf.nn.dropout(tf.nn.sigmoid(tf.nn.bias_add(z_matrix, z_bias)), keep_prob=(1 - dropout))

    z_mu = tf.nn.bias_add(tf.matmul(z_matrix, z_m_weights), z_m_bias)
    z_logvar = tf.nn.bias_add(tf.matmul(z_matrix, z_s_weights), z_s_bias)

    z_sample = _sample_z(z_mu, z_logvar)

    latent_matrix = tf.matmul(z_sample, latent_weights, transpose_b=True)
    latent_matrix = tf.nn.dropout(tf.nn.sigmoid(tf.nn.bias_add(latent_matrix, latent_bias)), keep_prob=(1 - dropout))

    new_sequence_matrix = tf.nn.bias_add(tf.matmul(latent_matrix, new_sequence_weights, transpose_b=True),
                                         new_sequence_bias)

    # np_z = np.random.randn(2*seq_size, z_size)
    # z = tf.convert_to_tensor(np_z, dtype=tf.float32)
    #
    # z_latent_matrix = tf.matmul(z, latent_weights, transpose_b=True)
    # z_latent_matrix = tf.nn.dropout(tf.nn.sigmoid(tf.nn.bias_add(z_latent_matrix, latent_bias)), keep_prob=(1 - dropout))
    #
    # x_sample_matrix = tf.nn.bias_add(tf.matmul(z_latent_matrix, new_sequence_weights, transpose_b=True),
    #                                  new_sequence_bias)
    # x_sample_matrix = tf.nn.sigmoid(x_sample_matrix)

    # reconstruction_loss = -tf.reduce_sum(x * tf.log(1e-10 + new_sequence_matrix) +
    #                                      (1 - x) * tf.log(1e-10 + 1 - new_sequence_matrix), 1)
    reconstruction_loss = tf.reduce_mean(tf.pow(x - new_sequence_matrix, 2))
    KL_divergence_loss = 1 / 2 * tf.reduce_sum(tf.exp(z_logvar) + z_mu ** 2 - 1 - z_logvar, axis=1)
    vae_loss = tf.reduce_mean(reconstruction_loss + KL_divergence_loss)

    # solver = tf.train.AdamOptimizer().minimize(vae_loss)

    return new_sequence_matrix, vae_loss


def _sample_z(mu, log_var):
    eps = tf.random_normal(shape=tf.shape(mu))
    return mu + tf.exp(log_var / 2) * eps


def cnn_decoder(final_hidden, output_size, is_training):
    if is_training:
        dropout = 0.4
    else:
        dropout = 0

    x = tf.expand_dims(final_hidden, 3)
    initializer = init[0]

    conv1 = tf.layers.conv2d(x, x.shape[1], [x.shape[1], 1], activation=gelu, kernel_initializer=initializer)
    conv1 = tf.nn.dropout(conv1, keep_prob=(1-dropout))
    conv1 = tf.reshape(tf.transpose(tf.squeeze(conv1), [0, 2, 1]), [-1, conv1.shape[-2]])

    conv2 = tf.layers.conv2d(x, x.shape[2], [1, x.shape[2]], activation=gelu, kernel_initializer=initializer)
    conv2 = tf.nn.dropout(conv2, keep_prob=(1 - dropout))
    conv2 = tf.reshape(tf.squeeze(conv2), [-1, conv2.shape[-1]])

    conv3 = tf.layers.conv2d(x, 1, [1, 1], activation=gelu, kernel_initializer=initializer)
    conv3 = tf.nn.dropout(conv3, keep_prob=(1 - dropout))
    conv3 = tf.reshape(tf.squeeze(conv3), [-1, conv3.shape[-2]])

    conv_123 = tf.matmul(tf.nn.softmax(tf.matmul(conv1, conv2, transpose_b=True)), conv3)
    conv4 = tf.layers.conv2d(tf.reshape(conv_123, [-1, x.shape[1], x.shape[2], 1]), 1, [1, 1],
                             activation=gelu, kernel_initializer=initializer)
    conv4 = tf.nn.dropout(conv4, keep_prob=(1 - dropout))

    conv_out = x + conv4

    logits = tf.squeeze(tf.layers.conv2d(conv_out, output_size, [1, x.shape[2]], kernel_initializer=initializer), 2)

    return logits


def att_decoder(final_hidden):
    """x: [b, s, h] """
    hidden_size = int(final_hidden.shape[-1])
    seq_len = int(final_hidden.shape[-2])
    initializer = init[0]

    final_hidden_matrix = tf.reshape(final_hidden, [-1, hidden_size])

    attention_weights = tf.get_variable(
        "cls/squad/attention_weights", [1, hidden_size],
        initializer=initializer)

    attention_bias = tf.get_variable(
        "cls/squad/attention_bias", [1], initializer=tf.zeros_initializer())

    output_weights = tf.get_variable(
        "cls/squad/output_weights", [2 * hidden_size, hidden_size],
        initializer=initializer)

    output_bias = tf.get_variable(
        "cls/squad/output_bias", [2 * hidden_size], initializer=tf.zeros_initializer())

    alpha = tf.nn.softmax(tf.reshape(
        tf.nn.bias_add(tf.matmul(final_hidden_matrix, attention_weights, transpose_b=True),
                       attention_bias), [-1, seq_len]), -1)

    attention_out = tf.squeeze(tf.matmul(tf.expand_dims(alpha, -1), final_hidden, transpose_a=True), 1)

    logits = tf.matmul(final_hidden, tf.reshape(
        tf.expand_dims(tf.nn.bias_add(tf.matmul(attention_out, output_weights, transpose_b=True),
                                      output_bias), -1), [-1, hidden_size, 2]))

    return logits


def gelu(input_tensor):
  """Gaussian Error Linear Unit.
  This is a smoother version of the RELU.
  Original paper: https://arxiv.org/abs/1606.08415
  Args:
    input_tensor: float Tensor to perform activation.

  Returns:
    `input_tensor` with the GELU activation applied.
  """
  cdf = 0.5 * (1.0 + tf.erf(input_tensor / tf.sqrt(2.0)))
  return input_tensor * cdf
