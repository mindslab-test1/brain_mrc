# MindsLab Inc. 
# Machine Reading Comprehender

import tensorflow as tf

import time, random, re, json, collections
import numpy as np
from pathlib import Path
from tqdm import tqdm

import evaluate_v1
import component
from component import SquadExample
from trfms.bert_google_tf1 import tokenization
from trfms import utils

class MrcDataProcessor():
    def __init__(self, cfg):
        self.cfg = cfg

        self.model_dir_path = Path(cfg.path.model_dir)

        self.tokenizer = tokenization.FullTokenizer(vocab_file=cfg.path.vocab_file, do_lower_case=cfg.model.do_lower_case)

    def build_train_input_fn(self, train_cfg):
        cfg = self.cfg
        model_cfg = cfg.model
        is_training = True      # Now unnecessary, but for future generalization into 'build_input_fn'

        # Load data file into a list of SquadExamples
        data_path = Path(train_cfg.data)
        input_data = json.loads(data_path.read_text())["data"]

        examples = load_squad_examples(
                input_data  = input_data, 
                is_training = is_training,
                version_2_with_negative = model_cfg.with_negative)

        if is_training:
            seed = int(time.time()) % cfg.seed_bucket
            random.Random(seed).shuffle(examples)

        # Tokenize and write data into a serialized tfrecord file to improve process time
        tfrecord_path = data_path.parent.joinpath(model_cfg.tokenizer, data_path.stem)
        if not tfrecord_path.exists():
            tfrecord_path.parent.mkdir(parents=True, exist_ok=True)

            component.file_based_convert_examples_to_features(
                    examples        = examples, 
                    tokenizer       = self.tokenizer,
                    output_file     = str(tfrecord_path),
                    max_seq_length  = model_cfg.max_seq_length, 
                    doc_stride      = model_cfg.doc_stride,
                    max_query_length= model_cfg.max_query_length,
                    is_training     = is_training,
                    version_2_with_negative = model_cfg.with_negative)

        # Build input_fn using tfrecord
        input_fn = component.file_based_input_fn_builder(
            input_file      = str(tfrecord_path),
            seq_length      = model_cfg.max_seq_length,
            is_training     = is_training,
            drop_remainder  = is_training)

        return input_fn, examples

    def build_infer_fn(self):
        model_cfg = self.cfg.model

        features = {
            'unique_ids':  tf.placeholder(tf.int32, shape=[None], name='unique_ids'),
            'input_ids':   tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='input_ids'),
            'input_mask':  tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='input_mask'),
            'segment_ids': tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='segment_ids'),
        }
        infer_fn = tf.estimator.export.build_raw_serving_input_receiver_fn(features)

        return infer_fn

    def build_test_examples(self, test_data_file):
        data_path = Path(test_data_file)
        input_data = json.loads(data_path.read_text())["data"]
        examples = load_squad_examples(input_data, is_training=False)
        return examples

    def build_infer_examples(self, contexts, questions):
        input_data = transform_text2squad(contexts, questions)
        examples = load_squad_examples(input_data, is_training=False)
        return examples

    def convert_examples_to_features(self, examples):
        model_cfg = self.cfg.model

        is_training = False
        features = component.convert_examples_to_features(examples, self.tokenizer, model_cfg.max_seq_length, 
                model_cfg.doc_stride, model_cfg.max_query_length, is_training, model_cfg.with_negative)

        return features

    def build_infer_batched_features(self, features):
        batched_features = {
            'unique_ids': [],
            'input_ids': [],
            'input_mask': [],
            'segment_ids': [],
        }
        for feature in features:
            batched_features['unique_ids'].append(feature.unique_id)
            batched_features['input_ids'].append(feature.input_ids)
            batched_features['input_mask'].append(feature.input_mask)
            batched_features['segment_ids'].append(feature.segment_ids)

        return batched_features

    def postprocess_batches(self, batched_outputs):
        RawResult = collections.namedtuple("RawResult", ["unique_id", "start_logits", "end_logits"])

        results = []
        for batched_output in batched_outputs:
            unique_id       = batched_output['unique_ids']
            start_logits    = batched_output['start_logits']
            end_logits      = batched_output['end_logits']

            if start_logits.ndim == 2:
                _results = [RawResult(_unique_id, _start_logits, _end_logits) 
                        for _unique_id, _start_logits, _end_logits in zip(unique_id, start_logits, end_logits)]
                results.extend(_results)
            elif start_logits.ndim == 1:
                results.append(RawResult(unique_id, start_logits, end_logits))

        return results
        
    def evaluate_result(self, examples, features, results):
        cfg = self.cfg
        model_cfg = cfg.model
        
        prediction_path = self.model_dir_path.joinpath(cfg.const.prediction)
        
        component.write_predictions(examples, features, results,
            model_cfg.n_best_size, model_cfg.max_answer_length, model_cfg.do_lower_case, 
            output_prediction_file=str(prediction_path), 
            output_nbest_file=None, 
            output_null_log_odds_file=None,
            version_2_with_negative=model_cfg.with_negative)

        test_data = json.loads(Path(cfg.infer.data).read_text())['data']
        predictions = json.loads(prediction_path.read_text())

        eval_result = evaluate_v1.evaluate(test_data, predictions)
        print(eval_result)

    def wrap_infer_results(self, examples, features, results):
        cfg = self.cfg
        model_cfg = cfg.model

        # if no output path is provided, it only returns predictions.
        answers = component.write_predictions(examples, features, results,
            model_cfg.n_best_size, model_cfg.max_answer_length, model_cfg.do_lower_case, 
            version_2_with_negative=model_cfg.with_negative)
        
        answers_list = []
        no_answers_list = []
        answers_scores = []
        for qas_id, qas in answers.items():
            batch_scores = []
            for qa in qas:
                qa.update({'passage_idx': int(qas_id)})
                if qa['text'] == '':
                    qa['text'] = '정답 없음'
                batch_scores.append(-1 * qa['probability'])
            if qas[np.argsort(batch_scores)[0]]['text'] == '정답 없음':
                no_answers_list.append(qas[np.argsort(batch_scores)[0]])
            else:
                for i in np.argsort(batch_scores)[:cfg.infer.top_k]:
                    answers_list.append(qas[i])
                    answers_scores.append(-1 * qas[i]['probability'])
        infer_results = [answers_list[i] for i in np.argsort(answers_scores)[:cfg.infer.top_k]] + no_answers_list

        return infer_results

def load_squad_examples(input_data, is_training, version_2_with_negative=False):
    """Read a SQuAD json file into a list of SquadExample."""
    examples = []
    for entry in input_data:
        for paragraph in entry["paragraphs"]:
            paragraph_text = paragraph["context"]
            doc_tokens = []
            char_to_word_offset = []
            prev_is_whitespace = True
            for c in paragraph_text:
                if utils.is_whitespace(c):
                    prev_is_whitespace = True
                else:
                    if prev_is_whitespace:
                        doc_tokens.append(c)
                    else:
                        doc_tokens[-1] += c
                    prev_is_whitespace = False
                char_to_word_offset.append(len(doc_tokens) - 1)

            for qa in paragraph["qas"]:
                qas_id = qa["id"]
                question_text = qa["question"]
                start_position = None
                end_position = None
                orig_answer_text = None
                is_impossible = False
                if is_training:
                    if version_2_with_negative:
                        is_impossible = qa["is_impossible"]
                    if (len(qa["answers"]) != 1) and (not is_impossible):
                        raise ValueError(
                            "For training, each question should have exactly 1 answer.")
                    if not is_impossible:
                        answer = qa["answers"][0]
                        orig_answer_text = answer["text"]
                        answer_offset = answer["answer_start"]
                        answer_length = len(orig_answer_text)
                        start_position = char_to_word_offset[answer_offset]
                        end_position = char_to_word_offset[answer_offset + answer_length - 1]
                        # Only add answers where the text can be exactly recovered from the
                        # document. If this CAN'T happen it's likely due to weird Unicode
                        # stuff so we will just skip the example.
                        #
                        # Note that this means for training mode, every example is NOT
                        # guaranteed to be preserved.
                        actual_text = " ".join(
                            doc_tokens[start_position:(end_position + 1)])
                        cleaned_answer_text = " ".join(
                            tokenization.whitespace_tokenize(orig_answer_text))
                        if actual_text.find(cleaned_answer_text) == -1:
                            tf.logging.warning("Could not find answer: '{}' vs. '{}'".format(actual_text, cleaned_answer_text))
                            continue
                    else:
                        start_position = -1
                        end_position = -1
                        orig_answer_text = ""

                example = SquadExample(
                    qas_id=qas_id,
                    question_text=question_text,
                    doc_tokens=doc_tokens,
                    orig_answer_text=orig_answer_text,
                    start_position=start_position,
                    end_position=end_position,
                    is_impossible=is_impossible)
                examples.append(example)

    return examples

def transform_text2squad(contexts, questions):
    data = []

    contexts = [contexts] if type(contexts) is str else contexts
    questions = [questions] if type(questions) is str else questions

    if len(contexts) == 1 and len(questions) > 1:
        contexts = contexts * len(questions)
    elif len(questions) == 1 and len(contexts) > 1:
        questions = questions * len(contexts)

    assert len(contexts) == len(questions)

    for idx, (context, question) in enumerate(zip(contexts, questions)):
        entry = {
            'title': None,
            'paragraphs': [{
                'context': context,
                'qas': [{
                    'id': str(idx),
                    'question': question,
                    'answers': [{
                        'text': None,
                        'answer_start': None
                    }]
                }]
            }]
        }
        data.append(entry)

    # squad_form = {'version': None, 'data': data}
    return data
