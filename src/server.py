# MindsLab Inc. 
# Machine Reading Comprehender

import argparse, logging, sys, time, grpc
from concurrent import futures
from datetime import datetime
from omegaconf import OmegaConf
from pathlib import Path

from runner import MrcRunner, setup_config, setup_logger, logger

from proto.mrc_pb2 import OutputText, Outputs
from proto.mrc_pb2_grpc import BertSquadServicer, add_BertSquadServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class BertSquad(BertSquadServicer):
    def __init__(self, cfg):
        self.runner = MrcRunner(cfg)

        logger.info('Warming up the GPU with a dummy data')
        _ = self.runner.infer(['Dummy context for warmup'], ['Dummy question'])
        logger.info('Done warming up')

    def Answer(self, msg, context):
        print(msg)
        results = self.runner.infer([context.context for context in msg.contexts], [msg.question])
        logger.debug(results)

        outputs = self._postprocess(results)

        return outputs

    def _postprocess(self, results):
        answers = [OutputText(passage_idx=result['passage_idx'],
                              text=result['text'],
                              prob=result['probability'],
                              start_index=result['start_index'],
                              end_index=result['end_index']) for result in results]

        outputs = Outputs(answers=answers)

        return outputs


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, required=True)
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    parser.add_argument('--port', type=int, required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    args.run = 'serve'  # it's a work-around. maybe there is a better way

    cfg = setup_config(args)
    setup_logger(cfg)

    logger.info('Initializing mrc engine')
    bertsquad = BertSquad(cfg)

    logger.info('Building grpc server')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_BertSquadServicer_to_server(bertsquad, server)

    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logger.info('Mrc Server starting at 0.0.0.0:{}'.format(args.port))

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        logger.info('Shutting down the server')
        server.stop(0)
