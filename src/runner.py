# MindsLab Inc. 
# Machine Reading Comprehender
#
# ver 1.1.1

import tensorflow as tf
from tensorflow.python.client import device_lib

import argparse, logging, shutil
from datetime import datetime
from tqdm import tqdm
from omegaconf import OmegaConf
from pathlib import Path

# engine modules
from trfms import find_architecture
from engine import MachineReadingComprehender as MrcEngine
from data_processor import MrcDataProcessor
import component

logger = logging.getLogger(__name__)

# a config file containing constants about paths & file names
CONST_CFG = 'cfg/const.yml'
const = OmegaConf.load(CONST_CFG)


# 1. Load data 
# 2. Build input_fn
# 3. Build estimator/predictor
# 4. Run train/test/inference
# 5. Post process
class MrcRunner(object):
    def __init__(self, cfg):
        self.cfg = cfg

        # instantiate essential objects with the given config
        architecture = find_architecture(cfg.model.architecture)
        self.data_processor = MrcDataProcessor(cfg)
        self.engine = MrcEngine(cfg, architecture)

        # for inferecne & serving. singleton pattern
        self.predictor = None   


    def train(self):
        train_cfg = self.cfg.train

        input_fn, inputs = self.data_processor.build_train_input_fn(train_cfg)
        train_steps = len(inputs)
        estimator = self.engine.build_estimator(train_cfg, train_steps)

        estimator.train(input_fn=input_fn, max_steps=train_steps)

    # Batch inference with test data
    def test(self, test_data_file):
        dp = self.data_processor
        examples = dp.build_test_examples(test_data_file)
        features = dp.convert_examples_to_features(examples)

        results = self._infer(features)

        dp.evaluate_result(examples, features, results) #TODO

    # Inference with input texts. for infer & serve
    # Return: list of #TODO
    def infer(self, contexts, questions):
        dp = self.data_processor
        examples = dp.build_infer_examples(contexts, questions)
        features = dp.convert_examples_to_features(examples)

        results = self._infer(features)
        infer_results = dp.wrap_infer_results(examples, features, results)
                 
        return infer_results

    # Used for both test & infer
    def _infer(self, inputs):
        cfg = self.cfg
        infer_cfg = cfg.infer

        dp = self.data_processor

        # Build predictor using infer_cfg
        if self.predictor is None:
            infer_fn = dp.build_infer_fn()
            self.predictor = self.engine.build_predictor(infer_cfg, infer_fn)

        batched_outputs = []
        for batch_beg in tqdm(range(0, len(inputs), infer_cfg.batch_size), desc='Processing inputs'):
            batch_end = batch_beg + infer_cfg.batch_size
            batched_input = inputs[batch_beg:batch_end] 

            batched_feature = dp.build_infer_batched_features(batched_input)
            batched_output = self.predictor(batched_feature)

            batched_outputs.append(batched_output)

        results = dp.postprocess_batches(batched_outputs)

        return results


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    # Arguments for running.
    parser.add_argument('--run', type=str, required=True, choices=['train', 'test', 'infer'])
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    # Configuration files for training
    parser.add_argument('--cfg', type=str)
    parser.add_argument('--model_cfg', type=str)

    # for test/inference. config files will be loaded from the model_path
    parser.add_argument('--model_path', type=str)
    parser.add_argument('--infer_context', type=str)
    parser.add_argument('--infer_question', type=str)

    args = parser.parse_args()
    return args

def setup_logger(cfg):
    log_file_name = '{}_{}.log'.format(datetime.now().strftime('%y%m%d_%H%M%S'), cfg.args.run)
    log_file_path = Path(cfg.const.log_dir, cfg.model_name, log_file_name)
    log_file_path.parent.mkdir(parents=True, exist_ok=True)

    logging.basicConfig(level=logging.DEBUG)
    formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(asctime)s] %(message)s')

    # create file handler
    fh = logging.FileHandler(str(log_file_path))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    # create stderr handler
    ch = logging.StreamHandler()    # stderr by default
    ch.setLevel(cfg.args.log_level)
    ch.setFormatter(formatter)
    # because 'tensorflow' logger output log to stderr, setting additional stream handler is not necessary.

    handlers = [fh, ch]
    logging.getLogger('').handlers = handlers
    logging.getLogger('tensorflow').handlers = []
    # logger.handlers = handlers

# Setup configuration
def setup_config(args):
    # Set model_dir_path
    if args.run == 'train':
        cfg = OmegaConf.load(args.cfg)
        model_dir_path = Path(const.models_dir, cfg.model_name)
        model_dir_path.mkdir(parents=True, exist_ok=True)
    else:
        model_path = Path(args.model_path)
        model_dir_path = model_path if model_path.is_dir() else model_path.parent

    # paths for essential files under the model dir
    cfg_file = str(model_dir_path.joinpath(const.run_cfg))
    model_cfg_file = str(model_dir_path.joinpath(const.model_cfg))
    vocab_file = str(model_dir_path.joinpath(const.vocab_file))

    # Pack & store essential configurations into the model dir.
    if args.run == 'train':
        shutil.copyfile(args.cfg, cfg_file)
        shutil.copyfile(args.model_cfg, model_cfg_file)
        shutil.copyfile(cfg.train.vocab_file, vocab_file)

    # Load configurations from the model dir
    cfg = OmegaConf.load(cfg_file)
    cfg.model = OmegaConf.load(model_cfg_file)
    cfg.const = const
    cfg.args = vars(args)

    # Set a proper checkpoint path
    # train: latest > pretrained
    # infer: model_path > latest
    latest_ckpt = tf.train.latest_checkpoint(str(model_dir_path))
    if args.run == 'train':
        model_ckpt = cfg.train.pretrained_ckpt if latest_ckpt is None else latest_ckpt
    else:
        model_ckpt = args.model_path if tf.train.checkpoint_exists(args.model_path) else latest_ckpt

    cfg.path = {}
    cfg.path.model_dir = str(model_dir_path)
    cfg.path.vocab_file = vocab_file
    cfg.path.init_checkpoint = model_ckpt

    return cfg


if __name__ == '__main__':
    args = parse_args()

    # Argument checking. Configs will be loaded in different ways depending on the run_phase.
    assert (args.run == 'train' and args.cfg != None and args.model_cfg != None)\
        or (args.run != 'train' and args.model_path != None)

    # Initial setup
    cfg = setup_config(args)
    setup_logger(cfg)

    # Initialize runner instance
    runner = MrcRunner(cfg)

    # Run a specific phase. Currently restricted for running only one phase at a time.
    if args.run == 'train':
        runner.train()

    elif args.run == 'test':
        runner.test(cfg.infer.data)

    elif args.run == 'infer':
        results = runner.infer(args.infer_context, args.infer_question)
        for result in results:
            print(result)

    print("Done running")
