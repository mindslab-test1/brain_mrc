# MindsLab Inc. 
# Machine Reading Comprehender

import grpc
import argparse
from proto import mrc_pb2
from proto import mrc_pb2_grpc

def request(host, port, passage, question, top_k):
    with grpc.insecure_channel('{}:{}'.format(host, port)) as channel:
        stub = mrc_pb2_grpc.BertSquadStub(channel)

        _contexts = [passage] if type(passage) == str else passage
        contexts = [mrc_pb2.Context(context=context) for context in _contexts]
        input_text = mrc_pb2.InputText(contexts=contexts, question=question, top_k=top_k)

        return stub.Answer(input_text)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, required=True)
    parser.add_argument('--passage', type=str, default="MRC 테스트용 문서입니다.")
    parser.add_argument('--question', type=str, default="MRC 테스트용 질문인가요?")
    parser.add_argument('--top_k', type=int, default=1)

    args = parser.parse_args()
    
    # Print the answers
    print('[Passage]:\n {}\n[Question]:\n {}\n'.format(args.passage, args.question))
    print('[Answers]')
    outputs = request(args.host, args.port, args.passage, args.question, args.top_k)
    for answer in outputs.answers:
        print('passage_idx: %i\nanswer: %s\nprob: %.4f\nstart_idx: %i\nend_idx: %i\n'
              % (answer.passage_idx, answer.text, answer.prob, answer.start_index, answer.end_index))
